export const environment = {
  production: true,
  version: '0.0.1',
  API_BASE_PATH: 'http://localhost:8081/api'
};
