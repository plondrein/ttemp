import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { AssumptionsComponent } from './assumptions/assumptions.component';
import { QuestionnaireWithResponsesComponent } from './questionnaire-responses/questionnaire-responses.component';
import { StudyWithAggResponsesComponent } from './study-agg-responses/study-agg-responses.component';
import { AboutComponent } from './about/about.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from './shared/guards/auth.guard';

const appRoutes: Routes = [
    // TODO: Add a landing page
    {
        path: '', canActivate: [AuthGuard], children: [
            { path: 'assumptions', component: AssumptionsComponent },
            { path: 'questionnaire', component: QuestionnaireWithResponsesComponent },
            { path: 'study', component: StudyWithAggResponsesComponent },
            { path: 'assumptions', component: AssumptionsComponent },
            { path: '', redirectTo: 'questionnaire', pathMatch: 'full' }
        ]
    },
    { path: 'about', component: AboutComponent },
    { path: 'not-found', component: ErrorPageComponent, data: { error_message: 'Page not found!' } },
    {
        path: 'unauthorized', component: ErrorPageComponent, data: {
            error_message: 'Unauthorized. Please use the full link provided by administrator.'
        }
    },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: false } // <-- set to true for debugging only
        )
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }
