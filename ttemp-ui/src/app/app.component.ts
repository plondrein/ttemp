import { Component } from '@angular/core';
import { DefaultService } from 'ttemp-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private apiGateway: DefaultService) { }
}
