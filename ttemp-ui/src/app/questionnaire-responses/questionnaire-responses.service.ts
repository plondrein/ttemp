import { Observable } from 'rxjs';
import { map, take, flatMap, mergeMap, zipAll } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { AssumptionWithResponseValue } from '@/shared/models/assumption-response.model';
import { AuthenticationService } from '@/shared/services/authentication.service';
import { ApiService } from '@/shared/services/api.service';
import { ResponseValue } from '@/shared/models/response-value.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireWithResponsesService {
  constructor(
    private authService: AuthenticationService,
    private apiService: ApiService) { }

  getQuestionnaireWithResponses(): Observable<AssumptionWithResponseValue[]> {
    const user = this.authService.appUser;

    return this.apiService.surveysGet(user.id, 'body')
      .pipe(
        flatMap(surveys => surveys),
        take(1),
        flatMap(survey => this.apiService.surveysSurveyIdAssumptionsGet(survey.id, 'body')),
        mergeMap(
          assumptions => assumptions.map(assumption =>
            this.apiService.usersUserIdSurveysSurveyIdAssumptionsAssumptionIdResponsesLatestGet(user.id, assumption.id).pipe(
              map(response => new AssumptionWithResponseValue(assumption, new ResponseValue(response.value)))
            )
          )
        ),
        zipAll()
      );
  }
}
