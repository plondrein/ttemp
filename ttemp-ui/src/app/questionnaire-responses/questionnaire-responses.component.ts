import { Component, OnInit, ViewChild } from '@angular/core';
import { AssumptionWithResponseValue } from '@/shared/models/assumption-response.model';
import { QuestionnaireWithResponsesService } from './questionnaire-responses.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-questionnaire-responses',
  templateUrl: './questionnaire-responses.component.html',
  styleUrls: ['./questionnaire-responses.component.css']
})
export class QuestionnaireWithResponsesComponent implements OnInit {
  assumptions: AssumptionWithResponseValue[] = [];

  @ViewChild('questionnaireForm')
  ngForm: NgForm;

  submitted = false;

  constructor(private questionnaireService: QuestionnaireWithResponsesService) { }

  ngOnInit() {
    this.questionnaireService.getQuestionnaireWithResponses()
      .subscribe((assumptions) => this.assumptions = assumptions);
  }

  onSubmit(): void {
    this.submitted = true;
    // TODO: Push update to backend
  }
}
