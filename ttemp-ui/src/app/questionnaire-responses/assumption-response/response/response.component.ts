import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['../../questionnaire-responses.component.css',
    './response.component.css']
})
export class ResponseComponent implements OnInit {
  @Input() response: boolean;
  @Input() isSelected: boolean;
  @Output() selected = new EventEmitter<boolean>();

  ngOnInit() {
  }

  public handleClick(event: any) {
    this.selected.emit(this.response);
  }
}
