import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

import { QuestionnaireWithResponsesComponent } from './questionnaire-responses.component';
import { AssumptionWithResponseComponent } from './assumption-response/assumption-response.component';
import { ResponseComponent } from './assumption-response/response/response.component';
import { AssumptionWithResponseValue } from '@/shared/models/assumption-response.model';
import { Assumption } from '@/shared/models/assumption.model';
import { ResponseValue } from '@/shared/models/response-value.model';
import { QuestionnaireWithResponsesService } from './questionnaire-responses.service';

const ASSUMPTIONS_RESPONSES: AssumptionWithResponseValue[] = [
    new AssumptionWithResponseValue(new Assumption(1, 'Assumption text 1'),
        new ResponseValue(null)),
    new AssumptionWithResponseValue(new Assumption(2, 'Assumption text 2'),
        new ResponseValue(true)),
    new AssumptionWithResponseValue(new Assumption(3, 'Assumption text 3'),
        new ResponseValue(false))
];

describe('QuestionnaireWithResponsesComponent', () => {
    let component: QuestionnaireWithResponsesComponent;
    let fixture: ComponentFixture<QuestionnaireWithResponsesComponent>;
    let questionnaireResponsesServiceSpy: jasmine.SpyObj<QuestionnaireWithResponsesService>;

    beforeEach(async(() => {
        questionnaireResponsesServiceSpy = jasmine.createSpyObj('QuestionnaireWithResponsesService', ['getQuestionnaireWithResponses']);
        questionnaireResponsesServiceSpy.getQuestionnaireWithResponses.and.returnValue(of(ASSUMPTIONS_RESPONSES));
        TestBed.configureTestingModule({
            declarations: [
                QuestionnaireWithResponsesComponent,
                AssumptionWithResponseComponent,
                ResponseComponent
            ],
            imports: [FormsModule],
            providers: [
                { provide: QuestionnaireWithResponsesService, useValue: questionnaireResponsesServiceSpy }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(QuestionnaireWithResponsesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create a spy for questionnaire service', () => {
        expect(questionnaireResponsesServiceSpy.getQuestionnaireWithResponses).toBeDefined();
    });

    it('should get assumptions from service', () => {
        expect(component.assumptions).toBe(ASSUMPTIONS_RESPONSES);
        expect(questionnaireResponsesServiceSpy.getQuestionnaireWithResponses).toHaveBeenCalledTimes(1);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show current responses', async(() => {
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            const responsePos = fixture.debugElement.query(By.css('.outer-circle.positive'));
            expect(responsePos).not.toBeNull('should show a positive response option');

            const responseNeg = fixture.debugElement.query(By.css('.outer-circle.negative'));
            expect(responseNeg).not.toBeNull('should show a negative response option');

            const responsePosSelected = fixture.debugElement.query(By.css('.inner-circle.positive'));
            expect(responsePosSelected).not.toBeNull('should show a positive response selected');

            const responseNegSelected = fixture.debugElement.query(By.css('.inner-circle.negative'));
            expect(responseNegSelected).not.toBeNull('should show a negative response selected');
        });
    }));

    it('form should be invalid', async(() => {
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(component.ngForm.valid).toBeFalsy();

            const submitBtn = fixture.nativeElement.querySelector('button[type=submit]');
            expect(submitBtn.disabled).toBeTruthy();
        });
    }));
});
