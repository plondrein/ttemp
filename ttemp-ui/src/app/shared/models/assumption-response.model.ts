import { Assumption } from '@/shared/models/assumption.model';
import { ResponseValue } from './response-value.model';

export class AssumptionWithResponseValue {
  constructor(
    public assumption: Assumption,
    public responseValue: ResponseValue) { }
}
