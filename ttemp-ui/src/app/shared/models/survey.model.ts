import { Survey as SurveyI } from 'ttemp-api';

export class Survey implements SurveyI {
    constructor(
        public id: number,
        public name: string,
        public start_datetime: Date,
        public end_datetime?: Date
    ) { }
}
