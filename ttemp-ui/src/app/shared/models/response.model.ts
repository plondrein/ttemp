import { Response as ResponseI } from 'ttemp-api';

export class Response implements ResponseI {
    constructor(
        public id: number,
        public survey_id: number,
        public assumption_id: number,
        public app_user_id: number,
        public value: boolean,
        public response_datetime: Date
    ) { }
}
