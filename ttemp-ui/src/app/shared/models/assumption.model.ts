import { Assumption as AssumptionI } from 'ttemp-api';

export class Assumption implements AssumptionI {

  constructor(
    public id: number,
    public assumption_text: string,
    public created_datetime?: Date) {
  }
}
