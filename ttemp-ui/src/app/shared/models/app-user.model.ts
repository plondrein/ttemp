import { AppUser as AppUserI } from 'ttemp-api';

export class AppUser implements AppUserI {
  constructor(
    public id: number,
    public login: string,
    public email: string,
    public user_uuid: string,
    public full_name?: string,
    public is_admin?: boolean
    ) { }
}
