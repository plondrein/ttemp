import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot,
        _: RouterStateSnapshot): Promise<boolean> {

        function redirect(router: Router) {
            router.navigate(['unauthorized']);
        }
        return new Promise(
            (resolve, __) => {
                const userToken = route.queryParams['id'];
                if (userToken) {
                    this.authenticationService.authenticate(userToken).then(
                        (isAuthenticated: boolean) => {
                            if (!isAuthenticated) {
                                redirect(this.router);
                            }
                            resolve(isAuthenticated);
                        });
                } else if (this.authenticationService.isAuthenticated) {
                    resolve(true);
                } else {
                    redirect(this.router);
                    resolve(false);
                }
            }
        );
    }
}
