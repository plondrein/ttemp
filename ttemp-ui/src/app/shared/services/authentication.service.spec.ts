
import { async, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AuthenticationService } from './authentication.service';
import { ApiService } from './api.service';
import { AppUser } from '@/shared/models/app-user.model';

const CORRECT_TOKEN = 'correct_token';
const INCORRECT_TOKEN = 'incorrect_token';

describe('AuthenticationService', () => {
    let service: AuthenticationService;
    let apiServiceSpy: jasmine.SpyObj<ApiService>;

    beforeEach(async(() => {
        apiServiceSpy = jasmine.createSpyObj('ApiService', ['usersGet']);
        apiServiceSpy.usersGet.withArgs(CORRECT_TOKEN).and
            .returnValue(of(new AppUser(1, 'testuser', 'example@example.com', CORRECT_TOKEN)));
        apiServiceSpy.usersGet.withArgs(INCORRECT_TOKEN).and
            .returnValue(of(null));

        TestBed.configureTestingModule({
            providers: [
                { provide: ApiService, useValue: apiServiceSpy }
            ]
        });
        service = TestBed.get(AuthenticationService);
    }));

    it('should create a spy for API Service', () => {
        expect(apiServiceSpy).toBeDefined();
    });

    it('should define a method for API Service', () => {
        expect(apiServiceSpy.usersGet).toBeDefined();
    });

    describe('authenticate', () => {
        let storage: {};

        beforeEach(async () => {
            storage = {};
            spyOn(localStorage, 'setItem').and.callFake((key: string, value: string) => { storage[key] = value; });
            spyOn(localStorage, 'getItem').and.callFake((key: string) => key in storage ? storage[key] : null);
            spyOn(localStorage, 'removeItem').and.callFake((key: string) => delete storage[key]);
        });

        it('should create', () => {
            expect(service.authenticate).toBeDefined();
        });

        it('should authenticate', (done: DoneFn) => {
            service.authenticate(CORRECT_TOKEN).then(
                (result) => {
                    expect(result).toEqual(true);
                    expect(service.isAuthenticated).toEqual(true);
                    expect(apiServiceSpy.usersGet).toHaveBeenCalled();
                    expect(localStorage.setItem).toHaveBeenCalledTimes(2);
                    done();
                }
            );
        });

        it('should not authenticate', (done: DoneFn) => {
            service.authenticate(INCORRECT_TOKEN).then(
                (result) => {
                    expect(result).toEqual(false);
                    expect(service.isAuthenticated).toEqual(false);
                    expect(localStorage.setItem).not.toHaveBeenCalled();
                    done();
                }
            );
        });

        it('should override token', (done: DoneFn) => {
            storage['user_token'] = 'old_token';
            service.authenticate(CORRECT_TOKEN).then(
                (result) => {
                    expect(result).toEqual(true);
                    expect(localStorage.setItem).toHaveBeenCalledTimes(2);
                    done();
                }
            );
        });

        it('should clear token', (done: DoneFn) => {
            storage['user_token'] = CORRECT_TOKEN;
            service.authenticate(INCORRECT_TOKEN).then(
                (result) => {
                    expect(result).toEqual(false);
                    expect(localStorage.removeItem).toHaveBeenCalledTimes(2);
                    expect(localStorage.setItem).not.toHaveBeenCalled();
                    done();
                }
            );
        });
    });
});
