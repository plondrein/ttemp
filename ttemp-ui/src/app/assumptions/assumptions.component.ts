import { Component, OnInit } from '@angular/core';

import { Assumption } from '@/shared/models/assumption.model';
import { DefaultService } from 'ttemp-api';

@Component({
  selector: 'app-assumptions',
  templateUrl: './assumptions.component.html',
  styleUrls: ['./assumptions.component.css']
})
export class AssumptionsComponent implements OnInit {
  assumptions: Assumption[];
  constructor(private apiGateway: DefaultService) { }

  ngOnInit() {
    this.apiGateway.assumptionsGet()
      .subscribe(assumptions => this.assumptions = <Assumption[]> assumptions);
  }

}
