import { Component, Input } from '@angular/core';
import { AssumptionWithAggResponses } from './assumption-agg-responses.model';

@Component({
  selector: 'app-assumption-agg-responses',
  templateUrl: './assumption-agg-responses.component.html',
  styleUrls: ['./assumption-agg-responses.component.css']
})
export class AssumptionWithAggResponsesComponent {
  @Input() assumptionWithAggResponses: AssumptionWithAggResponses;
}
