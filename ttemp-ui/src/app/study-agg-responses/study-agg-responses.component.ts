import { Component, OnInit } from '@angular/core';
import { AssumptionWithAggResponses } from './assumption-agg-responses/assumption-agg-responses.model';
import { StudyWithAggResponsesService } from './study-agg-responses.service';

@Component({
  selector: 'app-study-agg-responses',
  templateUrl: './study-agg-responses.component.html',
  styleUrls: ['./study-agg-responses.component.css']
})
export class StudyWithAggResponsesComponent implements OnInit {
  assumptions: AssumptionWithAggResponses[] = [];

  constructor(private studyService: StudyWithAggResponsesService) { }

  ngOnInit() {
    this.studyService.getStudyWithAggResponses()
      .subscribe((assumptions) => this.assumptions = assumptions);
  }
}
