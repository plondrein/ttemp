# Ttemp

Team temperature gathering tool

### Business goal

Main purpose of this product is to provide a tool for continuous team temperature gathering.
Agile teams can use it to measure satisfaction and quickly react if problems appear.

### Technical design goals

Main technical goal is to test API-first approach and generate significant parts of a server and client automatically from API specification.
Another goal is to learn and evaluate several languages, frameworks and technologies used to build modern web applications.

OpenAPI Specification (called Swagger before) was selected to define REST API for communication between backend and UI.
Version 2 of OpenAPI is currently used because many code generators didn't properly support v3 yet.

Golang was selected for a backend code.
Go-swagger is used to generate bakckend side of the API.
SQLBoiler is used in a "database-first" approach to generate entities and mappings based on a database schema.

Angular was selected as a web application framework for web UI.
OpenAPI Generator is used to generate Typescript Angualr code with service implementation and model.

PostgreSQL was selected to be used as a RDBMS.

Docker-compose is used for local deployment and all 3 main components (db, server, client) are build as separate containers.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Install:
* [Docker](https://www.docker.com/get-started) - Docker
* [Git](https://git-scm.com/downloads) - Git (also installs MinGW under Windows, so you'll have a shell)

### Installing

Clone git repo:
```
git clone https://gitlab.com/dympsz/ttemp.git
```

Go to project directory:
```
cd ttemp
```

Run everything (uses docker-compose):
```
./start_all.sh
```

## Running the tests

TODO!
Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Deployment

TODO!
Add additional notes about how to deploy this on a live system

## Built With

* [Angular](https://angular.io/docs) - the web framework
* [TypeScript](https://www.typescriptlang.org/) - programming language (for UI)
* [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) - OpenAPI generator to produce typescript-angular client
* [Go](https://golang.org/) - programming language (for backend)
* [Go-Swagger](https://github.com/go-swagger/go-swagger) - Go Swagger server generator
* [SQLBoiler](https://github.com/volatiletech/sqlboiler) - a tool to generate a Go ORM tailored to database schema
* [PostgreSQL](https://www.postgresql.org/) - relational database

## Contributing

Please read [CONTRIBUTING.md](TODO) for details on our code of conduct, and the process for submitting pull requests to us.

Additional info for developers is available here: [DEVELOPERS.md](DEVELOPERS.md)

## Communication

### Issue reporting

Please report all bugs and other issues here: [Ttemp issues](https://gitlab.com/dympsz/ttemp/issues)

### Online communication (currently only for contributors)

We have slack channel here: [Ttemp Slack channel](https://ttemp.slack.com)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/dympsz/ttemp/tags). 

## Authors

* **Dymitr Pszenicyn** - *Initial work & technical design* - [dympsz](https://gitlab.com/dympsz)
* **Andrzej Kacperski** - *Frontend* - [akacperski](https://gitlab.com/akacperski)
* **Tomasz Urbański** - *Original project idea* - [turbo74](https://gitlab.com/turbo74)

See also the list of [contributors](https://gitlab.com/dympsz/ttemp/graphs/master) who participated in this project.

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

* TODO

## Development plans

See the [TODO.md](TODO.md) file for details.
