#!/bin/sh

# This doesn't work because of docker-compose BUG: https://github.com/docker/compose/issues/3874
#docker-compose -f ttemp-srv/docker-compose.yml -f ttemp-ui/docker-compose.yml up

# HACK / workaround:
docker-compose -f ttemp-srv/docker-compose.yml config > _tmp_.srv.yml
docker-compose -f ttemp-ui/docker-compose.yml config > _tmp_.ui.yml
echo "# GENERATED FILE. DON'T EDIT!" > docker-compose.yml
docker-compose \
  -f _tmp_.srv.yml \
  -f _tmp_.ui.yml \
  config >> docker-compose.yml
rm _tmp_.*.yml

docker-compose up
