-- Ttemp DB sample data

-- create extension if not exists pgcrypto;
-- select gen_random_uuid();

-- admin;ttemp1
insert into app_user(id, login, email, full_name, user_uuid, is_admin, password_hash)
  values(1, 'admin', 'admin@example.com', 'Admin User', '95f4cdb4-e10d-4200-aa00-ede942dd3c76', true,
  '16384$8$1$ea5d13f698c350d03cfb12fea8f22806$68957ce0cb99a8446534230ea45260b28a476996a9f2220dfaff24d58615039e');
-- user1;abc123
insert into app_user(id, login, email, full_name, user_uuid, is_admin, password_hash)
  values(2, 'user1', 'user1@example.com', 'Test User 1', 'c82506e8-3def-4329-8d96-c461904eb35c', false,
  '16384$8$1$f7f872728154b60880aecce552ac6b15$bd8dc1a6e2855bd41a36615cc66608c088114e8cdd95714882b13d4a0ee50b4a');
-- user1;abc123
insert into app_user(id, login, email, full_name, user_uuid, is_admin, password_hash)
  values(3, 'user2', 'user2@example.com', 'Test User 2', '3f8b3031-ce62-41ce-8c02-399884038761', false,
  '16384$8$1$ebb3bbdbf7b683b260f473af22e680be$475d068d22e971f5c63a9d5a5b603df1ea560de351d854ed9977a611d269a690');

alter table app_user alter column id restart with 1000;

insert into survey(id, name, start_datetime) values(1, 'Test survery #1', '2019-01-01');
insert into survey(id, name, start_datetime) values(2, 'Test survery #2', '2019-01-02');

alter table survey alter column id restart with 1000;

insert into app_user_survey(app_user_id, survey_id) values(2, 1);
insert into app_user_survey(app_user_id, survey_id) values(3, 1);

insert into assumption(id, assumption_text, created_datetime) values(1, 'Assumption text #1', '2019-01-01');
insert into assumption(id, assumption_text, created_datetime) values(2, 'Assumption text #2', '2019-01-01');
insert into assumption(id, assumption_text, created_datetime) values(3, 'Assumption text #3', '2019-01-01');
insert into assumption(id, assumption_text, created_datetime) values(4, 'Assumption text #4', '2019-01-01');
insert into assumption(id, assumption_text, created_datetime) values(5, 'Assumption text #5', '2019-01-01');
insert into assumption(id, assumption_text, created_datetime) values(6, 'Assumption text #6', '2019-01-02');

alter table assumption alter column id restart with 1000;

insert into survey_assumption(survey_id, assumption_id) values(1, 1);
insert into survey_assumption(survey_id, assumption_id) values(1, 2);
insert into survey_assumption(survey_id, assumption_id) values(1, 3);
insert into survey_assumption(survey_id, assumption_id) values(1, 4);
insert into survey_assumption(survey_id, assumption_id) values(1, 5);

-- Responses for survey #1:
-- user1:
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(1, 1, 1, 2, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(2, 1, 2, 2, true, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(3, 1, 3, 2, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(4, 1, 4, 2, true, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(5, 1, 5, 2, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(6, 1, 1, 2, false, '2019-01-02');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(7, 1, 2, 2, true, '2019-01-02');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(8, 1, 3, 2, true, '2019-01-02');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(9, 1, 4, 2, true, '2019-01-02');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(10, 1, 5, 2, true, '2019-01-02');
-- user2:
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(11, 1, 1, 3, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(12, 1, 2, 3, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(13, 1, 3, 3, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(14, 1, 4, 3, true, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(15, 1, 5, 3, false, '2019-01-01');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(16, 1, 1, 3, true, '2019-01-03');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(17, 1, 2, 3, true, '2019-01-03');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(18, 1, 3, 3, true, '2019-01-03');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(19, 1, 4, 3, false, '2019-01-03');
insert into response(id, survey_id, assumption_id, app_user_id, value, response_datetime) values(20, 1, 5, 3, true, '2019-01-03');

alter table response alter column id restart with 1000;
