package datastore

import (
	"database/sql"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"
)

// Global DB... TODO: change to environment or something.
var db *sql.DB

func NewDB(connectionString string) (*sql.DB, error) {
	var err error
	db, err = sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	log.Info("Connected to DB")
	return db, nil
}

func CloseDB() {
	log.Info("Closing DB...")
	db.Close()
}
