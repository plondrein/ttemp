package datastore

import (
	"context"
	"errors"

	"github.com/volatiletech/sqlboiler/boil"

	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
)

func NewAssumption(assumption *models.Assumption) (int, error) {
	err := assumption.Insert(context.Background(), db, boil.Infer())
	return assumption.ID, err
}

func ListAssumptions() (models.AssumptionSlice, error) {
	assumptions, err := models.Assumptions().All(context.Background(), db)
	return assumptions, err
}

// mock version:
func ListAssumptionsMock() (models.AssumptionSlice, error) {
	assumptions := []*models.Assumption{
		&models.Assumption{ID: 1, AssumptionText: "Assumption 1"},
		&models.Assumption{ID: 2, AssumptionText: "Assumption 2"},
	}
	return assumptions, nil
}

func ListAssumptionsBySurveyID(surveyID int) (models.AssumptionSlice, error) {
	assumptions, err := models.Assumptions(
		qm.Select(),
		qm.InnerJoin("survey_assumption sa on sa.assumption_id = assumption.id"),
		qm.Where("survey_id = ?", surveyID)).All(context.Background(), db)

	return assumptions, err
}

func GetAssumption(id int) (*models.Assumption, error) {
	assumption, err := models.Assumptions(qm.Where("id = ?", id)).One(context.Background(), db)
	if err != nil {
		return nil, err
	} else if assumption == nil {
		return nil, errors.New("assumption not found")
	} else {
		return assumption, nil
	}
}

func UpdateAssumption(assumption *models.Assumption) error {
	_, err := assumption.Update(context.Background(), db, boil.Infer())
	return err
}

func DeleteAssumption(assumption *models.Assumption) error {
	_, err := assumption.Delete(context.Background(), db)
	return err
}
