package datastore

import (
	"context"
	"errors"

	"github.com/volatiletech/sqlboiler/boil"

	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
)

func NewSurvey(survey *models.Survey) (int, error) {
	err := survey.Insert(context.Background(), db, boil.Infer())
	return survey.ID, err
}

func ListSurveys() (models.SurveySlice, error) {
	surveys, err := models.Surveys().All(context.Background(), db)
	return surveys, err
}

func ListSurveysByUserID(userID int) (models.SurveySlice, error) {
	surveys, err := models.Surveys(qm.Where("user_id = ?", userID)).All(context.Background(), db)
	return surveys, err
}

func GetSurvey(id int) (*models.Survey, error) {
	survey, err := models.Surveys(qm.Where("id = ?", id)).One(context.Background(), db)
	if err != nil {
		return nil, err
	} else if survey == nil {
		return nil, errors.New("survey not found")
	} else {
		return survey, nil
	}
}

func UpdateSurvey(survey *models.Survey) error {
	_, err := survey.Update(context.Background(), db, boil.Infer())
	return err
}

func DeleteSurvey(survey *models.Survey) error {
	_, err := survey.Delete(context.Background(), db)
	return err
}
