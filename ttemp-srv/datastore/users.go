package datastore

import (
	"context"
	"errors"

	"github.com/volatiletech/sqlboiler/boil"

	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
)

func NewUser(user *models.AppUser) (int, error) {
	err := user.Insert(context.Background(), db, boil.Infer())
	return user.ID, err
}

func ListUsers() (models.AppUserSlice, error) {
	users, err := models.AppUsers().All(context.Background(), db)
	return users, err
}

func ListUsersByUserUUID(userUUID string) (models.AppUserSlice, error) {
	users, err := models.AppUsers(qm.Where("user_uuid = ?", userUUID)).All(context.Background(), db)
	return users, err
}

func GetUser(id int) (*models.AppUser, error) {
	user, err := models.AppUsers(qm.Where("id = ?", id)).One(context.Background(), db)
	if err != nil {
		return nil, err
	} else if user == nil {
		return nil, errors.New("user not found")
	} else {
		return user, nil
	}
}

func GetUserByLogin(login string) (*models.AppUser, error) {
	user, err := models.AppUsers(qm.Where("login = ?", login)).One(context.Background(), db)
	if err != nil {
		return nil, err
	} else if user == nil {
		return nil, errors.New("user not found")
	} else {
		return user, nil
	}
}

func UpdateUser(user *models.AppUser) error {
	_, err := user.Update(context.Background(), db, boil.Infer())
	return err
}

func DeleteUser(user *models.AppUser) error {
	_, err := user.Delete(context.Background(), db)
	return err
}
