package handlers

import (
	errors "github.com/go-openapi/errors"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"

	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitAuth(api *operations.TtempAPI) {
	api.AuthTokenAuth = authTokenAuth

	// Set your custom authorizer if needed. Default one is security.Authorized()
	// Expected interface runtime.Authorizer
	// Example:
	// api.APIAuthorizer = security.Authorized()
}

var authTokenAuth = func(token string) (*apimodels.AppUser, error) {
	log.Debug("auth token:", token)
	users, err := datastore.ListUsersByUserUUID(token)
	if err == nil && len(users) == 1 {
		apiuser := DBUserToAPIUser(users[0])
		return apiuser, nil
	}

	log.Error("Access attempt with incorrect auth token:", token)
	return nil, errors.New(401, "incorrect auth token")
}
