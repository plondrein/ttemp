package handlers

import (
	"time"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
	dbmodels "gitlab.com/dympsz/ttemp/ttemp-srv/models"

	"github.com/go-openapi/runtime/middleware"
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitAssumptionHandlers(api *operations.TtempAPI) {
	api.GetAssumptionsHandler = getAssumptionsHandler
	api.PostAssumptionsHandler = postAssumptionsHandler
	api.DeleteAssumptionsAssumptionIDHandler = deleteAssumptionsAssumptionIDHandler
	api.GetAssumptionsAssumptionIDHandler = getAssumptionsAssumptionIDHandler
	api.PutAssumptionsAssumptionIDHandler = putAssumptionsAssumptionIDHandler
}

var getAssumptionsHandler = operations.GetAssumptionsHandlerFunc(
	func(params operations.GetAssumptionsParams, apiUser *apimodels.AppUser) middleware.Responder {

		var assumptions models.AssumptionSlice
		var err error
		if params.SurveyID == nil {
			assumptions, err = datastore.ListAssumptions()
		} else {
			surveyID := swag.Int64Value(params.SurveyID)
			log.Debug("surveyId", surveyID)
			assumptions, err = datastore.ListAssumptionsBySurveyID(int(surveyID))
		}
		if err != nil {
			log.Error("getAssumptionsHandler", err)
			return operations.NewGetAssumptionsDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(assumptions), "assumptions")

		payload := DBAssumptionListToAPIAssumptionList(assumptions)
		return operations.NewGetAssumptionsOK().WithPayload(payload)
	})

var postAssumptionsHandler = operations.PostAssumptionsHandlerFunc(
	func(params operations.PostAssumptionsParams, apiUser *apimodels.AppUser) middleware.Responder {
		assumption := params.Body
		dbAssumption := APIAssumptionToDBAssumption(assumption)

		_, err := datastore.NewAssumption(dbAssumption)
		if err != nil {
			log.Error("postAssumptionsHandler", err)
			return operations.NewPostAssumptionsDefault(500).WithPayload(modelsError(err))
		}

		payload := DBAssumptionToAPIAssumption(dbAssumption)
		return operations.NewPostAssumptionsCreated().WithPayload(payload)
	})

var deleteAssumptionsAssumptionIDHandler = operations.DeleteAssumptionsAssumptionIDHandlerFunc(
	func(params operations.DeleteAssumptionsAssumptionIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		assumptionID := int(params.AssumptionID)

		assumption, err := datastore.GetAssumption(assumptionID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("deleteAssumptionsAssumptionIDHandler", err)
			return operations.NewDeleteAssumptionsAssumptionIDDefault(500).WithPayload(modelsError(err))
		}

		err = datastore.DeleteAssumption(assumption)
		if err != nil {
			log.Error("deleteAssumptionsAssumptionIDHandler", err)
			return operations.NewDeleteAssumptionsAssumptionIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewDeleteAssumptionsAssumptionIDNoContent()
	})

var getAssumptionsAssumptionIDHandler = operations.GetAssumptionsAssumptionIDHandlerFunc(
	func(params operations.GetAssumptionsAssumptionIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		assumptionID := int(params.AssumptionID)

		assumption, err := datastore.GetAssumption(assumptionID)
		if err != nil {
			log.Error("getAssumptionsAssumptionIDHandler", err)
			return operations.NewGetAssumptionsAssumptionIDDefault(500).WithPayload(modelsError(err))
		}

		payload := DBAssumptionToAPIAssumption(assumption)
		return operations.NewGetAssumptionsAssumptionIDOK().WithPayload(payload)
	})

var putAssumptionsAssumptionIDHandler = operations.PutAssumptionsAssumptionIDHandlerFunc(
	func(params operations.PutAssumptionsAssumptionIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		assumptionID := int(params.AssumptionID)
		newAssumption := params.Body

		newDBAssumption := APIAssumptionToDBAssumption(newAssumption)

		dbAssumption, err := datastore.GetAssumption(assumptionID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("putAssumptionsAssumptionIDHandler", err)
			return operations.NewPutAssumptionsAssumptionIDDefault(500).WithPayload(modelsError(err))
		}

		if dbAssumption.ID != newDBAssumption.ID {
			return operations.NewPutAssumptionsAssumptionIDDefault(400).WithPayload(
				modelsErrorStringWithCode("Assumption ID doesn't match", 400))
		}

		err = datastore.UpdateAssumption(newDBAssumption)
		if err != nil {
			log.Error("putAssumptionsAssumptionIDHandler", err)
			return operations.NewPutAssumptionsAssumptionIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewPutAssumptionsAssumptionIDNoContent()
	})

// Conversion functions below

func DBAssumptionToAPIAssumption(assumption *dbmodels.Assumption) *apimodels.Assumption {
	result := apimodels.Assumption{}
	result.AssumptionText = swag.String(assumption.AssumptionText)
	result.CreatedDatetime = strfmt.DateTime(assumption.CreatedDatetime)
	result.ID = int64(assumption.ID)
	return &result
}

func DBAssumptionListToAPIAssumptionList(assumptions []*dbmodels.Assumption) []*apimodels.Assumption {
	list := []*apimodels.Assumption{}
	for _, dbAssumption := range assumptions {
		list = append(list, DBAssumptionToAPIAssumption(dbAssumption))
	}
	return list
}

func APIAssumptionToDBAssumption(assumption *apimodels.Assumption) *dbmodels.Assumption {
	result := dbmodels.Assumption{}
	result.AssumptionText = swag.StringValue(assumption.AssumptionText)
	result.CreatedDatetime = time.Time(assumption.CreatedDatetime)
	result.ID = int(assumption.ID)
	return &result
}
