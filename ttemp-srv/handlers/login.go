package handlers

import (
	"gitlab.com/dympsz/ttemp/ttemp-srv/crypt"
	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"
	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitLoginHandlers(api *operations.TtempAPI) {
	api.PostLoginHandler = postLoginHandler
}

var postLoginHandler = operations.PostLoginHandlerFunc(
	func(params operations.PostLoginParams) middleware.Responder {

		log.Debug("login", params.Login)
		user, err := datastore.GetUserByLogin(params.Login)
		if err != nil {
			log.Error("postLoginHandler", err)
			if err.Error() == "sql: no rows in result set" {
				return operations.NewPostLoginUnauthorized()
			} else {
				return operations.NewPostLoginDefault(500).WithPayload(modelsError(err))
			}
		}

		// check password
		err = crypt.CheckPasswordWithHash(params.Password, user.PasswordHash.Bytes)
		if err != nil {
			log.Error("postLoginHandler", err)
			// TODO: set WithWWWAuthenticate as well
			// return operations.NewPostLoginUnauthorized().WithPayload(modelsError(err))
			return operations.NewPostLoginUnauthorized()
		}

		// TODO: don't use UUID as a token and use proper Auth!
		XAuthToken := user.UserUUID
		return operations.NewPostLoginOK().WithXAuthToken(XAuthToken)
	})
