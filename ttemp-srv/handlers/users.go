package handlers

import (
	"time"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"github.com/volatiletech/null"
	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
	dbmodels "gitlab.com/dympsz/ttemp/ttemp-srv/models"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitUserHandlers(api *operations.TtempAPI) {
	api.GetUsersHandler = getUsersHandler
	api.PostUsersHandler = postUsersHandler
	api.DeleteUsersUserIDHandler = deleteUsersUserIDHandler
	api.GetUsersUserIDHandler = getUsersUserIDHandler
	api.PutUsersUserIDHandler = putUsersUserIDHandler

	api.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler
	api.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandler = getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandler
}

var getUsersHandler = operations.GetUsersHandlerFunc(
	func(params operations.GetUsersParams, apiUser *apimodels.AppUser) middleware.Responder {

		var users models.AppUserSlice
		var err error
		if params.UUID == nil {
			users, err = datastore.ListUsers()
		} else {
			userUUID := swag.StringValue(params.UUID)
			log.Debug("uuid", userUUID)
			users, err = datastore.ListUsersByUserUUID(userUUID)
		}
		if err != nil {
			log.Error("getUsersHandler", err)
			return operations.NewGetUsersDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(users), "users")

		payload := DBUserListToAPIUserList(users)
		return operations.NewGetUsersOK().WithPayload(payload)
	})

var postUsersHandler = operations.PostUsersHandlerFunc(
	func(params operations.PostUsersParams, apiUser *apimodels.AppUser) middleware.Responder {
		user := params.Body
		dbUser := APIUserToDBUser(user)

		_, err := datastore.NewUser(dbUser)
		if err != nil {
			log.Error("postUsersHandler", err)
			return operations.NewPostUsersDefault(500).WithPayload(modelsError(err))
		}

		payload := DBUserToAPIUser(dbUser)
		return operations.NewPostUsersCreated().WithPayload(payload)
	})

var deleteUsersUserIDHandler = operations.DeleteUsersUserIDHandlerFunc(
	func(params operations.DeleteUsersUserIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		userID := int(params.UserID)

		user, err := datastore.GetUser(userID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("deleteUsersUserIDHandler", err)
			return operations.NewDeleteUsersUserIDDefault(500).WithPayload(modelsError(err))
		}

		err = datastore.DeleteUser(user)
		if err != nil {
			log.Error("deleteUsersUserIDHandler", err)
			return operations.NewDeleteUsersUserIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewDeleteUsersUserIDNoContent()
	})

var getUsersUserIDHandler = operations.GetUsersUserIDHandlerFunc(
	func(params operations.GetUsersUserIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		userID := int(params.UserID)

		user, err := datastore.GetUser(userID)
		if err != nil {
			log.Error("getUsersUserIDHandler", err)
			return operations.NewGetUsersUserIDDefault(500).WithPayload(modelsError(err))
		}

		payload := DBUserToAPIUser(user)
		return operations.NewGetUsersUserIDOK().WithPayload(payload)
	})

var putUsersUserIDHandler = operations.PutUsersUserIDHandlerFunc(
	func(params operations.PutUsersUserIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		userID := int(params.UserID)
		newUser := params.Body

		newDBUser := APIUserToDBUser(newUser)

		dbUser, err := datastore.GetUser(userID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("putUsersUserIDHandler", err)
			return operations.NewPutUsersUserIDDefault(500).WithPayload(modelsError(err))
		}

		if dbUser.ID != newDBUser.ID {
			return operations.NewPutUsersUserIDDefault(400).WithPayload(
				modelsErrorStringWithCode("User ID doesn't match", 400))
		}

		err = datastore.UpdateUser(newDBUser)
		if err != nil {
			log.Error("putUsersUserIDHandler", err)
			return operations.NewPutUsersUserIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewPutUsersUserIDNoContent()
	})

var getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandlerFunc(
	func(params operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesParams, apiUser *apimodels.AppUser) middleware.Responder {
		userID := int(params.UserID)
		surveyID := int(params.SurveyID)
		assumptionID := int(params.AssumptionID)

		responses, err := datastore.ListResponsesWithFilters(&userID, &surveyID, &assumptionID)
		if err != nil {
			log.Error("getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler", err)
			return operations.NewGetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(responses), "responses")

		payload := DBResponseListToAPIResponseList(responses)
		return operations.NewGetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesOK().WithPayload(payload)
	})

var getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandler = operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandlerFunc(
	func(params operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestParams, apiUser *apimodels.AppUser) middleware.Responder {
		userID := int(params.UserID)
		surveyID := int(params.SurveyID)
		assumptionID := int(params.AssumptionID)
		var beforeDateTime *time.Time = nil
		if params.BeforeDatetime != nil {
			tmp := time.Time(*params.BeforeDatetime)
			beforeDateTime = &tmp
		}
		var response dbmodels.Response
		var err error
		if beforeDateTime != nil {
			response, err = datastore.ListResponsesWithFiltersLatestBeforeDT(userID, surveyID, assumptionID, *beforeDateTime)
		} else {
			response, err = datastore.ListResponsesWithFiltersLatest(userID, surveyID, assumptionID)
		}
		if err != nil {
			log.Error("getUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandler", err)
			if err.Error() == "sql: no rows in result set" {
				return operations.NewGetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestNotFound()
			}
			return operations.NewGetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestDefault(500).WithPayload(modelsError(err))
		}

		payload := DBResponseToAPIResponse(&response)
		return operations.NewGetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestOK().WithPayload(payload)
	})

// Conversion functions below

func DBUserToAPIUser(user *dbmodels.AppUser) *apimodels.AppUser {
	result := apimodels.AppUser{}
	result.Email = user.Email.Ptr()
	result.FullName = user.FullName.Ptr()
	result.ID = int64(user.ID)
	result.Login = swag.String(user.Login)
	// result.PasswordHash = swag.String(user.PasswordHash)
	result.UserUUID = user.UserUUID
	return &result
}

func DBUserListToAPIUserList(users []*dbmodels.AppUser) []*apimodels.AppUser {
	list := []*apimodels.AppUser{}
	for _, dbUser := range users {
		list = append(list, DBUserToAPIUser(dbUser))
	}
	return list
}

func APIUserToDBUser(user *apimodels.AppUser) *dbmodels.AppUser {
	result := dbmodels.AppUser{}
	result.Email = null.StringFromPtr(user.Email)
	result.FullName = null.StringFromPtr(user.FullName)
	result.ID = int(user.ID)
	result.Login = swag.StringValue(user.Login)
	// result.PasswordHash = swag.StringValue(user.PasswordHash)
	result.UserUUID = user.UserUUID
	return &result
}
