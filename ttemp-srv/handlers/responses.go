package handlers

import (
	"time"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
	dbmodels "gitlab.com/dympsz/ttemp/ttemp-srv/models"

	"github.com/go-openapi/runtime/middleware"
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitResponseHandlers(api *operations.TtempAPI) {
	api.GetResponsesHandler = getResponsesHandler
	api.PostResponsesHandler = postResponsesHandler
	api.DeleteResponsesResponseIDHandler = deleteResponsesResponseIDHandler
	api.GetResponsesResponseIDHandler = getResponsesResponseIDHandler
	api.PutResponsesResponseIDHandler = putResponsesResponseIDHandler
}

var getResponsesHandler = operations.GetResponsesHandlerFunc(
	func(params operations.GetResponsesParams, apiUser *apimodels.AppUser) middleware.Responder {

		var responses models.ResponseSlice
		var err error
		if params.UserID == nil && params.SurveyID == nil && params.AssumptionID == nil {
			responses, err = datastore.ListResponses()
		} else {
			var userID *int
			if params.UserID != nil {
				tmp := int(*params.UserID)
				userID = &tmp
			}
			var surveyID *int
			if params.SurveyID != nil {
				tmp := int(*params.SurveyID)
				surveyID = &tmp
			}
			var assumptionID *int
			if params.AssumptionID != nil {
				tmp := int(*params.AssumptionID)
				assumptionID = &tmp
			}

			responses, err = datastore.ListResponsesWithFilters(userID, surveyID, assumptionID)
		}
		if err != nil {
			log.Error("getResponsesHandler", err)
			return operations.NewGetResponsesDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(responses), "responses")

		payload := DBResponseListToAPIResponseList(responses)
		return operations.NewGetResponsesOK().WithPayload(payload)
	})

var postResponsesHandler = operations.PostResponsesHandlerFunc(
	func(params operations.PostResponsesParams, apiUser *apimodels.AppUser) middleware.Responder {
		response := params.Body
		dbResponse := APIResponseToDBResponse(response)

		_, err := datastore.NewResponse(dbResponse)
		if err != nil {
			log.Error("postResponsesHandler", err)
			return operations.NewPostResponsesDefault(500).WithPayload(modelsError(err))
		}

		payload := DBResponseToAPIResponse(dbResponse)
		return operations.NewPostResponsesCreated().WithPayload(payload)
	})

var deleteResponsesResponseIDHandler = operations.DeleteResponsesResponseIDHandlerFunc(
	func(params operations.DeleteResponsesResponseIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		responseID := int(params.ResponseID)

		response, err := datastore.GetResponse(responseID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("deleteResponsesResponseIDHandler", err)
			return operations.NewDeleteResponsesResponseIDDefault(500).WithPayload(modelsError(err))
		}

		err = datastore.DeleteResponse(response)
		if err != nil {
			log.Error("deleteResponsesResponseIDHandler", err)
			return operations.NewDeleteResponsesResponseIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewDeleteResponsesResponseIDNoContent()
	})

var getResponsesResponseIDHandler = operations.GetResponsesResponseIDHandlerFunc(
	func(params operations.GetResponsesResponseIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		responseID := int(params.ResponseID)

		response, err := datastore.GetResponse(responseID)
		if err != nil {
			log.Error("getResponsesResponseIDHandler", err)
			return operations.NewGetResponsesResponseIDDefault(500).WithPayload(modelsError(err))
		}

		payload := DBResponseToAPIResponse(response)
		return operations.NewGetResponsesResponseIDOK().WithPayload(payload)
	})

var putResponsesResponseIDHandler = operations.PutResponsesResponseIDHandlerFunc(
	func(params operations.PutResponsesResponseIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		responseID := int(params.ResponseID)
		newResponse := params.Body

		newDBResponse := APIResponseToDBResponse(newResponse)

		dbResponse, err := datastore.GetResponse(responseID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("putResponsesResponseIDHandler", err)
			return operations.NewPutResponsesResponseIDDefault(500).WithPayload(modelsError(err))
		}

		if dbResponse.ID != newDBResponse.ID {
			return operations.NewPutResponsesResponseIDDefault(400).WithPayload(
				modelsErrorStringWithCode("Response ID doesn't match", 400))
		}

		err = datastore.UpdateResponse(newDBResponse)
		if err != nil {
			log.Error("putResponsesResponseIDHandler", err)
			return operations.NewPutResponsesResponseIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewPutResponsesResponseIDNoContent()
	})

// Conversion functions below

func DBResponseToAPIResponse(response *dbmodels.Response) *apimodels.Response {
	result := apimodels.Response{}
	result.ID = int64(response.ID)
	result.SurveyID = swag.Int64(int64(response.SurveyID))
	result.AssumptionID = swag.Int64(int64(response.AssumptionID))
	result.AppUserID = swag.Int64(int64(response.AppUserID))
	result.Value = &response.Value
	result.ResponseDatetime = strfmt.DateTime(response.ResponseDatetime)
	return &result
}

func DBResponseListToAPIResponseList(responses []*dbmodels.Response) []*apimodels.Response {
	list := []*apimodels.Response{}
	for _, dbResponse := range responses {
		list = append(list, DBResponseToAPIResponse(dbResponse))
	}
	return list
}

func APIResponseToDBResponse(response *apimodels.Response) *dbmodels.Response {
	result := dbmodels.Response{}
	result.ID = int(response.ID)
	result.SurveyID = int(swag.Int64Value(response.SurveyID))
	result.AssumptionID = int(swag.Int64Value(response.AssumptionID))
	result.AppUserID = int(swag.Int64Value(response.AppUserID))
	result.Value = swag.BoolValue(response.Value)
	result.ResponseDatetime = time.Time(response.ResponseDatetime)
	return &result
}
