package handlers

import (
	"github.com/go-openapi/swag"
	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

func modelsError(err error) *apimodels.Error {
	return &apimodels.Error{
		Message: swag.String(err.Error()),
		Code:    swag.Int64(500),
	}
}

func modelsErrorWithCode(err error, code int) *apimodels.Error {
	return &apimodels.Error{
		Message: swag.String(err.Error()),
		Code:    swag.Int64(int64(code)),
	}
}

func modelsErrorStringWithCode(str string, code int) *apimodels.Error {
	return &apimodels.Error{
		Message: swag.String(str),
		Code:    swag.Int64(int64(code)),
	}
}
