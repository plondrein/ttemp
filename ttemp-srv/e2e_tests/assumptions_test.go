package e2etests

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAssumptions(t *testing.T) {
	assert := assert.New(t)

	url := baseURL + "/assumptions"

	resp, err := sendGet(url)

	assert.Nil(err, "Should not return error")
	assert.NotNil(resp, "Response should exits")

	defer resp.Body.Close()

	assert.Equal(200, resp.StatusCode, "StatusCode should be 200")

	body, _ := ioutil.ReadAll(resp.Body)
	assert.NotEmpty(string(body), "Response body should not be empty")
	t.Log(string(body))
	// TODO: check actual contents of response
}

func TestGetAssumptions1(t *testing.T) {
	assert := assert.New(t)

	url := baseURL + "/assumptions/1"

	resp, err := sendGet(url)

	assert.Nil(err, "Should not return error")
	assert.NotNil(resp, "Response should exits")

	defer resp.Body.Close()

	assert.Equal(200, resp.StatusCode, "StatusCode should be 200")

	body, _ := ioutil.ReadAll(resp.Body)
	assert.NotEmpty(string(body), "Response body should not be empty")
	t.Log(string(body))
	// TODO: check actual contents of response
}

// TODO: more tests for assumptions!
