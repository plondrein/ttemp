package e2etests

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPostLoginOk(t *testing.T) {
	assert := assert.New(t)

	url := baseURL + "/login"

	req, err := http.NewRequest("POST", url, http.NoBody)
	assert.Nil(err, "Should not return error")

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("login", "user1")
	req.Header.Set("password", "abc123")

	resp, err := sendRequest(req)

	assert.Nil(err, "Should not return error")
	assert.NotNil(resp, "Response should exits")

	defer resp.Body.Close()

	assert.Equal(200, resp.StatusCode, "StatusCode should be 200")

	assert.Equal(authTokenUser1, resp.Header["X-Auth-Token"][0], "Should get auth token")
}

func TestPostLoginUnknownUser(t *testing.T) {
	assert := assert.New(t)

	url := baseURL + "/login"

	req, err := http.NewRequest("POST", url, http.NoBody)
	assert.Nil(err, "Should not return error")

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("login", "WrongUser")
	req.Header.Set("password", "WrongPassword")

	resp, err := sendRequest(req)

	assert.Nil(err, "Should not return error")
	assert.NotNil(resp, "Response should exits")

	defer resp.Body.Close()

	assert.Equal(401, resp.StatusCode, "StatusCode should be 401")
	assert.Nil(resp.Header["X-Auth-Token"], "Should not get auth token")
}

func TestPostLoginWrongPassword(t *testing.T) {
	assert := assert.New(t)

	url := baseURL + "/login"

	req, err := http.NewRequest("POST", url, http.NoBody)
	assert.Nil(err, "Should not return error")

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("login", "user1")
	req.Header.Set("password", "WrongPassword")

	resp, err := sendRequest(req)

	assert.Nil(err, "Should not return error")
	assert.NotNil(resp, "Response should exits")

	defer resp.Body.Close()

	assert.Equal(401, resp.StatusCode, "StatusCode should be 401")
	assert.Nil(resp.Header["X-Auth-Token"], "Should not get auth token")
}
