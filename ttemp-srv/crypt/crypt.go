package crypt

import (
	scrypt "github.com/elithrar/simple-scrypt"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"
)

func PasswordToHash(password string) ([]byte, error) {
	hash, err := scrypt.GenerateFromPassword([]byte(password), scrypt.DefaultParams)
	if err != nil {
		log.Debug("Calculate password hash error: ", err)
	} else {
		log.Debug("Calculate password hash: ", hash)
	}
	return hash, err
}

func CheckPasswordWithHash(password string, hash []byte) error {
	err := scrypt.CompareHashAndPassword(hash, []byte(password))
	if err != nil {
		log.Debug("Compare password and hash error: ", err)
	}
	return err
}
