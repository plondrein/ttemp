// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// DeleteUsersUserIDHandlerFunc turns a function with the right signature into a delete users user ID handler
type DeleteUsersUserIDHandlerFunc func(DeleteUsersUserIDParams, *models.AppUser) middleware.Responder

// Handle executing the request and returning a response
func (fn DeleteUsersUserIDHandlerFunc) Handle(params DeleteUsersUserIDParams, principal *models.AppUser) middleware.Responder {
	return fn(params, principal)
}

// DeleteUsersUserIDHandler interface for that can handle valid delete users user ID params
type DeleteUsersUserIDHandler interface {
	Handle(DeleteUsersUserIDParams, *models.AppUser) middleware.Responder
}

// NewDeleteUsersUserID creates a new http.Handler for the delete users user ID operation
func NewDeleteUsersUserID(ctx *middleware.Context, handler DeleteUsersUserIDHandler) *DeleteUsersUserID {
	return &DeleteUsersUserID{Context: ctx, Handler: handler}
}

/*DeleteUsersUserID swagger:route DELETE /users/{userId} deleteUsersUserId

Delete user

*/
type DeleteUsersUserID struct {
	Context *middleware.Context
	Handler DeleteUsersUserIDHandler
}

func (o *DeleteUsersUserID) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewDeleteUsersUserIDParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal *models.AppUser
	if uprinc != nil {
		principal = uprinc.(*models.AppUser) // this is really a models.AppUser, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
