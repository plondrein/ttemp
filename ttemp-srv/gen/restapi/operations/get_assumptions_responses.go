// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// GetAssumptionsOKCode is the HTTP code returned for type GetAssumptionsOK
const GetAssumptionsOKCode int = 200

/*GetAssumptionsOK List of assumptions.

swagger:response getAssumptionsOK
*/
type GetAssumptionsOK struct {

	/*
	  In: Body
	*/
	Payload []*models.Assumption `json:"body,omitempty"`
}

// NewGetAssumptionsOK creates GetAssumptionsOK with default headers values
func NewGetAssumptionsOK() *GetAssumptionsOK {

	return &GetAssumptionsOK{}
}

// WithPayload adds the payload to the get assumptions o k response
func (o *GetAssumptionsOK) WithPayload(payload []*models.Assumption) *GetAssumptionsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get assumptions o k response
func (o *GetAssumptionsOK) SetPayload(payload []*models.Assumption) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetAssumptionsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.Assumption, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// GetAssumptionsNotFoundCode is the HTTP code returned for type GetAssumptionsNotFound
const GetAssumptionsNotFoundCode int = 404

/*GetAssumptionsNotFound Error - Not found

swagger:response getAssumptionsNotFound
*/
type GetAssumptionsNotFound struct {
	/*The request id this is a response to

	 */
	XRequestID string `json:"X-Request-Id"`

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetAssumptionsNotFound creates GetAssumptionsNotFound with default headers values
func NewGetAssumptionsNotFound() *GetAssumptionsNotFound {

	return &GetAssumptionsNotFound{}
}

// WithXRequestID adds the xRequestId to the get assumptions not found response
func (o *GetAssumptionsNotFound) WithXRequestID(xRequestID string) *GetAssumptionsNotFound {
	o.XRequestID = xRequestID
	return o
}

// SetXRequestID sets the xRequestId to the get assumptions not found response
func (o *GetAssumptionsNotFound) SetXRequestID(xRequestID string) {
	o.XRequestID = xRequestID
}

// WithPayload adds the payload to the get assumptions not found response
func (o *GetAssumptionsNotFound) WithPayload(payload *models.Error) *GetAssumptionsNotFound {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get assumptions not found response
func (o *GetAssumptionsNotFound) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetAssumptionsNotFound) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	// response header X-Request-Id

	xRequestID := o.XRequestID
	if xRequestID != "" {
		rw.Header().Set("X-Request-Id", xRequestID)
	}

	rw.WriteHeader(404)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

/*GetAssumptionsDefault Error

swagger:response getAssumptionsDefault
*/
type GetAssumptionsDefault struct {
	_statusCode int
	/*The request id this is a response to

	 */
	XRequestID string `json:"X-Request-Id"`

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewGetAssumptionsDefault creates GetAssumptionsDefault with default headers values
func NewGetAssumptionsDefault(code int) *GetAssumptionsDefault {
	if code <= 0 {
		code = 500
	}

	return &GetAssumptionsDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the get assumptions default response
func (o *GetAssumptionsDefault) WithStatusCode(code int) *GetAssumptionsDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the get assumptions default response
func (o *GetAssumptionsDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithXRequestID adds the xRequestId to the get assumptions default response
func (o *GetAssumptionsDefault) WithXRequestID(xRequestID string) *GetAssumptionsDefault {
	o.XRequestID = xRequestID
	return o
}

// SetXRequestID sets the xRequestId to the get assumptions default response
func (o *GetAssumptionsDefault) SetXRequestID(xRequestID string) {
	o.XRequestID = xRequestID
}

// WithPayload adds the payload to the get assumptions default response
func (o *GetAssumptionsDefault) WithPayload(payload *models.Error) *GetAssumptionsDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get assumptions default response
func (o *GetAssumptionsDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetAssumptionsDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	// response header X-Request-Id

	xRequestID := o.XRequestID
	if xRequestID != "" {
		rw.Header().Set("X-Request-Id", xRequestID)
	}

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
