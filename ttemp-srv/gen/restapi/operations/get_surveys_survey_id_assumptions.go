// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// GetSurveysSurveyIDAssumptionsHandlerFunc turns a function with the right signature into a get surveys survey ID assumptions handler
type GetSurveysSurveyIDAssumptionsHandlerFunc func(GetSurveysSurveyIDAssumptionsParams, *models.AppUser) middleware.Responder

// Handle executing the request and returning a response
func (fn GetSurveysSurveyIDAssumptionsHandlerFunc) Handle(params GetSurveysSurveyIDAssumptionsParams, principal *models.AppUser) middleware.Responder {
	return fn(params, principal)
}

// GetSurveysSurveyIDAssumptionsHandler interface for that can handle valid get surveys survey ID assumptions params
type GetSurveysSurveyIDAssumptionsHandler interface {
	Handle(GetSurveysSurveyIDAssumptionsParams, *models.AppUser) middleware.Responder
}

// NewGetSurveysSurveyIDAssumptions creates a new http.Handler for the get surveys survey ID assumptions operation
func NewGetSurveysSurveyIDAssumptions(ctx *middleware.Context, handler GetSurveysSurveyIDAssumptionsHandler) *GetSurveysSurveyIDAssumptions {
	return &GetSurveysSurveyIDAssumptions{Context: ctx, Handler: handler}
}

/*GetSurveysSurveyIDAssumptions swagger:route GET /surveys/{surveyId}/assumptions getSurveysSurveyIdAssumptions

List assumptions in survey

*/
type GetSurveysSurveyIDAssumptions struct {
	Context *middleware.Context
	Handler GetSurveysSurveyIDAssumptionsHandler
}

func (o *GetSurveysSurveyIDAssumptions) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewGetSurveysSurveyIDAssumptionsParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal *models.AppUser
	if uprinc != nil {
		principal = uprinc.(*models.AppUser) // this is really a models.AppUser, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
