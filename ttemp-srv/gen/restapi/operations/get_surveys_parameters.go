// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetSurveysParams creates a new GetSurveysParams object
// no default values defined in spec.
func NewGetSurveysParams() GetSurveysParams {

	return GetSurveysParams{}
}

// GetSurveysParams contains all the bound params for the get surveys operation
// typically these are obtained from a http.Request
//
// swagger:parameters GetSurveys
type GetSurveysParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*Filter by user ID
	  In: query
	*/
	UserID *int64
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewGetSurveysParams() beforehand.
func (o *GetSurveysParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	qs := runtime.Values(r.URL.Query())

	qUserID, qhkUserID, _ := qs.GetOK("userId")
	if err := o.bindUserID(qUserID, qhkUserID, route.Formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// bindUserID binds and validates parameter UserID from query.
func (o *GetSurveysParams) bindUserID(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: false
	// AllowEmptyValue: false
	if raw == "" { // empty values pass all other validations
		return nil
	}

	value, err := swag.ConvertInt64(raw)
	if err != nil {
		return errors.InvalidType("userId", "query", "int64", raw)
	}
	o.UserID = &value

	return nil
}
