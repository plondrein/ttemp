// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	errors "github.com/go-openapi/errors"
	runtime "github.com/go-openapi/runtime"
	"github.com/rs/cors"

	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"
)

//go:generate swagger generate server --target ..\..\gen --name Ttemp --spec ..\..\api\Ttemp_swagger2.yaml --principal models.AppUser --exclude-main

func configureFlags(api *operations.TtempAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.TtempAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	// Applies when the "X-auth-token" header is set
	// api.AuthTokenAuth = func(token string) (*models.AppUser, error) {
	// 	return nil, errors.NotImplemented("api key auth (AuthToken) X-auth-token from header param [X-auth-token] has not yet been implemented")
	// }
	//
	// Set your custom authorizer if needed. Default one is security.Authorized()
	// Expected interface runtime.Authorizer
	// Example:
	// api.APIAuthorizer = security.Authorized()
	//
	// api.DeleteAssumptionsAssumptionIDHandler = operations.DeleteAssumptionsAssumptionIDHandlerFunc(func(params operations.DeleteAssumptionsAssumptionIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .DeleteAssumptionsAssumptionID has not yet been implemented")
	// })
	// api.DeleteResponsesResponseIDHandler = operations.DeleteResponsesResponseIDHandlerFunc(func(params operations.DeleteResponsesResponseIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .DeleteResponsesResponseID has not yet been implemented")
	// })
	// api.DeleteSurveysSurveyIDHandler = operations.DeleteSurveysSurveyIDHandlerFunc(func(params operations.DeleteSurveysSurveyIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .DeleteSurveysSurveyID has not yet been implemented")
	// })
	// api.DeleteUsersUserIDHandler = operations.DeleteUsersUserIDHandlerFunc(func(params operations.DeleteUsersUserIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .DeleteUsersUserID has not yet been implemented")
	// })
	// api.GetAssumptionsHandler = operations.GetAssumptionsHandlerFunc(func(params operations.GetAssumptionsParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetAssumptions has not yet been implemented")
	// })
	// api.GetAssumptionsAssumptionIDHandler = operations.GetAssumptionsAssumptionIDHandlerFunc(func(params operations.GetAssumptionsAssumptionIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetAssumptionsAssumptionID has not yet been implemented")
	// })
	// api.GetResponsesHandler = operations.GetResponsesHandlerFunc(func(params operations.GetResponsesParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetResponses has not yet been implemented")
	// })
	// api.GetResponsesResponseIDHandler = operations.GetResponsesResponseIDHandlerFunc(func(params operations.GetResponsesResponseIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetResponsesResponseID has not yet been implemented")
	// })
	// api.GetSurveysHandler = operations.GetSurveysHandlerFunc(func(params operations.GetSurveysParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetSurveys has not yet been implemented")
	// })
	// api.GetSurveysSurveyIDHandler = operations.GetSurveysSurveyIDHandlerFunc(func(params operations.GetSurveysSurveyIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetSurveysSurveyID has not yet been implemented")
	// })
	// api.GetSurveysSurveyIDAssumptionsHandler = operations.GetSurveysSurveyIDAssumptionsHandlerFunc(func(params operations.GetSurveysSurveyIDAssumptionsParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetSurveysSurveyIDAssumptions has not yet been implemented")
	// })
	// api.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesHandlerFunc(func(params operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetSurveysSurveyIDAssumptionsAssumptionIDResponses has not yet been implemented")
	// })
	// api.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler = operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandlerFunc(func(params operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser has not yet been implemented")
	// })
	// api.GetUsersHandler = operations.GetUsersHandlerFunc(func(params operations.GetUsersParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetUsers has not yet been implemented")
	// })
	// api.GetUsersUserIDHandler = operations.GetUsersUserIDHandlerFunc(func(params operations.GetUsersUserIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetUsersUserID has not yet been implemented")
	// })
	// api.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesHandlerFunc(func(params operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponses has not yet been implemented")
	// })
	// api.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandler = operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestHandlerFunc(func(params operations.GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .GetUsersUserIDSurveysSurveyIDAssumptionsAssumptionIDResponsesLatest has not yet been implemented")
	// })
	// api.PostAssumptionsHandler = operations.PostAssumptionsHandlerFunc(func(params operations.PostAssumptionsParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PostAssumptions has not yet been implemented")
	// })
	// api.PostLoginHandler = operations.PostLoginHandlerFunc(func(params operations.PostLoginParams) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PostLogin has not yet been implemented")
	// })
	// api.PostResponsesHandler = operations.PostResponsesHandlerFunc(func(params operations.PostResponsesParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PostResponses has not yet been implemented")
	// })
	// api.PostSurveysHandler = operations.PostSurveysHandlerFunc(func(params operations.PostSurveysParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PostSurveys has not yet been implemented")
	// })
	// api.PostUsersHandler = operations.PostUsersHandlerFunc(func(params operations.PostUsersParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PostUsers has not yet been implemented")
	// })
	// api.PutAssumptionsAssumptionIDHandler = operations.PutAssumptionsAssumptionIDHandlerFunc(func(params operations.PutAssumptionsAssumptionIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PutAssumptionsAssumptionID has not yet been implemented")
	// })
	// api.PutResponsesResponseIDHandler = operations.PutResponsesResponseIDHandlerFunc(func(params operations.PutResponsesResponseIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PutResponsesResponseID has not yet been implemented")
	// })
	// api.PutSurveysSurveyIDHandler = operations.PutSurveysSurveyIDHandlerFunc(func(params operations.PutSurveysSurveyIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PutSurveysSurveyID has not yet been implemented")
	// })
	// api.PutUsersUserIDHandler = operations.PutUsersUserIDHandlerFunc(func(params operations.PutUsersUserIDParams, principal *models.AppUser) middleware.Responder {
	// 	return middleware.NotImplemented("operation .PutUsersUserID has not yet been implemented")
	// })

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	// handleCORS := cors.Default().Handler // default CORS
	// return handler // no CORS

	handleCORS := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "X-auth-token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
		Debug:            true,
	}).Handler

	return handleCORS(handler)
}
