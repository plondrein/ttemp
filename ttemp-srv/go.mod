module gitlab.com/dympsz/ttemp/ttemp-srv

require (
	github.com/elithrar/simple-scrypt v1.3.0
	github.com/go-openapi/analysis v0.18.0 // indirect
	github.com/go-openapi/errors v0.18.0
	github.com/go-openapi/loads v0.18.0
	github.com/go-openapi/runtime v0.18.0
	github.com/go-openapi/spec v0.18.0
	github.com/go-openapi/strfmt v0.18.0
	github.com/go-openapi/swag v0.18.0
	github.com/go-openapi/validate v0.18.0
	github.com/gofrs/uuid v3.1.0+incompatible // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.0
	github.com/rs/cors v1.6.0
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.0 // indirect
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.3.0
	github.com/ugorji/go/codec v0.0.0-20181209151446-772ced7fd4c2 // indirect
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.1.0+incompatible
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3
	golang.org/x/sys v0.0.0-20190102155601-82a175fd1598 // indirect
)
